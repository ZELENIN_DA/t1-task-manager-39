package ru.t1.dzelenin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    M add(M model);

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(Comparator<M> comparator);

    @Nullable
    boolean existsById(@NotNull String id);

    @Nullable
    M findOneById(String id);

    @Nullable
    M findOneByIndex(Integer index);

    @Nullable
    M removeOneById(String id);

    @Nullable
    M removeOneByIndex(Integer index);

    @NotNull
    M removeOne(M model);

    void removeAll(Collection<M> collection);

    void removeAll();

    Integer getSize();

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

}


