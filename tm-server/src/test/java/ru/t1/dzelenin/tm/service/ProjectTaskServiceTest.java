package ru.t1.dzelenin.tm.service;

import org.junit.experimental.categories.Category;
import ru.t1.dzelenin.tm.marker.UnitCategory;

@Category(UnitCategory.class)
public final class ProjectTaskServiceTest {
/*
    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Before
    public void before() {
        projectRepository.add(USER1_PROJECT1);
        projectRepository.add(USER1_PROJECT2);
        taskRepository.add(USER1_TASK1);
        taskRepository.add(USER1_TASK2);
    }

    @After
    public void after() {
        taskRepository.removeAll(TASK_LIST);
        projectRepository.removeAll(PROJECT_LIST);
    }

    @Test
    public void bindTaskToProject() {
        projectTaskService.bindTaskToProject(USER1.getId(), USER1_PROJECT1.getId(), USER1_TASK1.getId());
        Assert.assertEquals(USER1_PROJECT1.getId(), USER1_TASK1.getProjectId());
        projectTaskService.bindTaskToProject(USER1.getId(), USER1_PROJECT2.getId(), USER1_TASK2.getId());
        Assert.assertEquals(USER1_PROJECT2.getId(), USER1_TASK2.getProjectId());
    }

    @Test
    public void unbindTaskFromProject() {
        bindTaskToProject();
        Assert.assertNotNull(USER1_TASK1.getProjectId());
        projectTaskService.unbindTaskFromProject(USER1.getId(), USER1_PROJECT1.getId(), USER1_TASK1.getId());
        Assert.assertNull(USER1_TASK1.getProjectId());
    }

    @Test
    public void removeProjectById() {
        bindTaskToProject();
        projectTaskService.removeProjectById(USER1.getId(), USER1_PROJECT1.getId());
        Assert.assertNull(projectRepository.findOneById(USER1_PROJECT1.getId()));
        Assert.assertNull(taskRepository.findOneById(USER1_TASK1.getId()));
    }
*/
}
