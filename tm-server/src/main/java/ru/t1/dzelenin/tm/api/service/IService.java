package ru.t1.dzelenin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.api.repository.IRepository;
import ru.t1.dzelenin.tm.model.AbstractModel;

import java.util.Collection;

public interface IService<M extends AbstractModel> extends IRepository<M> {

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

}


