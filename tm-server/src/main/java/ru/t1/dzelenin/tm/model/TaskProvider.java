package ru.t1.dzelenin.tm.model;

import org.mybatis.dynamic.sql.SqlColumn;
import ru.t1.dzelenin.tm.enumerated.Status;

import java.util.Date;

public class TaskProvider {

    public static final TaskSqlTable task = new TaskSqlTable();

    public static final SqlColumn<String> id = task.id;

    public static final SqlColumn<String> userId = task.userId;

    public static final SqlColumn<String> name = task.name;

    public static final SqlColumn<String> description = task.description;

    public static final SqlColumn<String> projectId = task.projectId;

    public static final SqlColumn<Status> status = task.status;

    public static final SqlColumn<Date> created = task.created;

    public static final SqlColumn<Date> dateBegin = task.dateBegin;

    public static final SqlColumn<Date> dateEnd = task.dateEnd;

}
