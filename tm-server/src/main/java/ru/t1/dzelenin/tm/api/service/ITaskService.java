package ru.t1.dzelenin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.enumerated.Status;
import ru.t1.dzelenin.tm.enumerated.TaskSort;
import ru.t1.dzelenin.tm.model.Task;

import java.util.Date;
import java.util.List;

public interface ITaskService extends IUserOwnerService<Task> {

    void create(@Nullable String userId, @Nullable String name);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    void create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description,
            @Nullable Date dateBegin,
            @Nullable Date dateEnd
    );

    @NotNull
    List<Task> findAll(@Nullable String userId, @Nullable TaskSort sort);

    @NotNull
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @NotNull
    Task updateById(@Nullable String userId,
                    @Nullable String id,
                    @Nullable String name,
                    @Nullable String description);

    @NotNull
    Task changeTaskStatusId(@Nullable String userId, @Nullable String id, @Nullable Status status);

}


