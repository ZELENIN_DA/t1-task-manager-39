package ru.t1.dzelenin.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.api.repository.IUserRepository;
import ru.t1.dzelenin.tm.api.service.IConnectionService;
import ru.t1.dzelenin.tm.api.service.IPropertyService;
import ru.t1.dzelenin.tm.api.service.IUserService;
import ru.t1.dzelenin.tm.enumerated.Role;
import ru.t1.dzelenin.tm.exception.entity.UserNotFoundException;
import ru.t1.dzelenin.tm.exception.field.EmailEmptyException;
import ru.t1.dzelenin.tm.exception.field.IdEmptyException;
import ru.t1.dzelenin.tm.exception.field.LoginEmptyException;
import ru.t1.dzelenin.tm.exception.field.PasswordEmptyException;
import ru.t1.dzelenin.tm.exception.user.ExistsEmailException;
import ru.t1.dzelenin.tm.exception.user.ExistsLoginException;
import ru.t1.dzelenin.tm.model.User;
import ru.t1.dzelenin.tm.util.HashUtil;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    private final IPropertyService propertyService;

    public UserService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IPropertyService propertyService
    ) {
        super(connectionService);
        this.propertyService = propertyService;
    }


    @NotNull
    @Override
    @SneakyThrows
    public User create(@NotNull final String login, @NotNull final String password) {
        if (login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final SqlSession sqlSession = getSession();
        @NotNull final User user = new User();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            user.setLogin(login);
            @NotNull final String secret = propertyService.getPasswordSecret();
            @NotNull final Integer iteration = propertyService.getPasswordIteration();
            user.setPasswordHash(HashUtil.salt(password, secret, iteration));
            user.setRole(Role.USUAL);
            repository.add(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final String email
    ) {
        if (login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password.isEmpty()) throw new PasswordEmptyException();
        if (isMailExist(email)) throw new ExistsEmailException();
        @NotNull final SqlSession sqlSession = getSession();
        @NotNull final User user = new User();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            user.setLogin(login);
            @NotNull final String secret = propertyService.getPasswordSecret();
            @NotNull final Integer iteration = propertyService.getPasswordIteration();
            user.setPasswordHash(HashUtil.salt(password, secret, iteration));
            user.setRole(Role.USUAL);
            repository.add(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final Role role
    ) {
        if (login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final SqlSession sqlSession = getSession();
        @NotNull final User user = new User();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            user.setLogin(login);
            @NotNull final String secret = propertyService.getPasswordSecret();
            @NotNull final Integer iteration = propertyService.getPasswordIteration();
            if (role == null) user.setRole(Role.USUAL);
            else user.setRole(role);
            repository.add(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new LoginEmptyException();
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            return repository.findOneByLogin(login);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@NotNull final String email) {
        if (email.isEmpty()) throw new EmailEmptyException();
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            return repository.findOneByEmail(email);
        }
    }

    @NotNull
    @Override
    public void removeByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new LoginEmptyException();
        @NotNull final User user = Optional.ofNullable(findByLogin(login))
                .orElseThrow(UserNotFoundException::new);
    }

    @NotNull
    @Override
    public void removeByEmail(@NotNull final String email) {
        if (email.isEmpty()) throw new EmailEmptyException();
        @NotNull final User user = Optional.ofNullable(findByEmail(email))
                .orElseThrow(UserNotFoundException::new);
    }

    @Override
    public User setPassword(@NotNull final String id, @NotNull final String password) {
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        update(user);
        return user;
    }

    @Override
    public User updateUser(
            @NotNull final String id,
            @NotNull final String firstName,
            @NotNull final String lastName,
            @NotNull final String middleName
    ) {
        @NotNull final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        update(user);
        return user;
    }

    @Nullable
    @Override
    public Boolean isLoginExist(@NotNull final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return findByLogin(login) != null;
    }

    @Nullable
    @Override
    public Boolean isMailExist(@NotNull final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return findByEmail(email) != null;
    }

    @Override
    public void lockUserByLogin(@NotNull final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        update(user);
    }

    @Override
    public void unlockUserByLogin(@NotNull final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        update(user);
    }

    @Override
    public void add(@NotNull final User model) {
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.add(model);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    public Collection<User> set(@NotNull final Collection<User> models) {
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.clear();
            repository.addAll(models);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return models;
    }

    @Override
    public void clear() {
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.clear();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public List<User> findAll() {
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            return repository.findAll();
        }
    }

    @Nullable
    @Override
    public User findOneById(@NotNull String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            return repository.findOneById(id);
        }
    }

    @Override
    public boolean existsById(@NotNull String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            return repository.existsById(id);
        }
    }

    @Override
    public void remove(@NotNull User model) {
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.remove(model);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeById(@NotNull String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = getSession();
        @Nullable final User user;
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            user = repository.findOneById(id);
            if (user == null) return;
            repository.remove(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void update(@NotNull User model) {
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.update(model);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public long getCount() {
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            return repository.getCount();
        }
    }

}
