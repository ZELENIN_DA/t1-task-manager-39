package ru.t1.dzelenin.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import ru.t1.dzelenin.tm.model.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @Insert("INSERT INTO taskmanager.project (id, created, name, description, status, user_id, date_begin, date_end)"
            + "VALUES (#{id}, #{created}, #{name}, #{description}, #{status}, #{userId}, #{dateBegin}, #{dateEnd})")
    void add(@NotNull Project project);

    @Insert("INSERT INTO taskmanager.project (id, created, name, description, status, user_id, date_begin, date_end)"
            + "VALUES (#{id}, #{created}, #{name}, #{description}, #{status}, #{userId}, #{dateBegin}, #{dateEnd})")
    void addAll(@NotNull Collection<Project> projects);

    @Delete("DELETE FROM taskmanager.project")
    void clear();

    @Delete("DELETE FROM taskmanager.project WHERE user_id = #{userId}")
    void clearByUserId(@Param("userId") @Nullable String userId);

    @NotNull
    @Select("SELECT id, created, name, description, status, user_id, date_begin, date_end FROM taskmanager.project")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    List<Project> findAll();

    @NotNull
    @Select("SELECT id, created, name, description, status, user_id, date_begin, date_end FROM taskmanager.project "
            + "WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    List<Project> findAllByUserId(@Param("userId") @Nullable String userId);

    @NotNull
    @SelectProvider(type = SqlProviderAdapter.class, method = "select")
    List<Project> findAllComparatorByUserId(SelectStatementProvider selectStatementProvider);

    @Select("SELECT count(1) = 1 FROM taskmanager.project WHERE id = #{id}")
    boolean existsById(@Param("id") @NotNull String id);

    @Select("SELECT count(1) = 1 FROM taskmanager.project WHERE user_id = #{userId} AND id = #{id}")
    boolean existsByIdAndUserId(@Param("userId") @Nullable String userId, @Param("id") @Nullable String id);

    @Nullable
    @Select("SELECT id, created, name, description, status, user_id, date_begin, date_end FROM taskmanager.project "
            + "WHERE id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    Project findOneById(@Param("id") @NotNull String id);

    @Nullable
    @Select("SELECT id, created, name, description, status, user_id, date_begin, date_end FROM taskmanager.project "
            + "WHERE user_id = #{userId} AND id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    Project findOneByIdAndUserId(@Param("userId") @Nullable String userId, @Param("id") @Nullable String id);

    @Delete("DELETE FROM taskmanager.project WHERE id = #{id}")
    void remove(@NotNull Project project);

    @Delete("DELETE FROM taskmanager.project WHERE user_id = #{userId} AND id = #{id}")
    void removeAll(@Nullable Collection<Project> collection);

    @Update("UPDATE taskmanager.project SET created = #{created}, name = #{name}, description = #{description}, "
            + "status = #{status}, date_begin = #{dateBegin}, date_end = #{dateEnd} "
            + "WHERE id = #{id}")
    void update(@NotNull Project project);

    @Select("SELECT count(1) FROM taskmanager.project")
    long getCount();

    @Select("SELECT count(1) FROM taskmanager.project WHERE user_id = #{userId}")
    long getCountByUserId(@Param("userId") @Nullable String userId);

}
