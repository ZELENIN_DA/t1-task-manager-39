package ru.t1.dzelenin.tm.api.service;

import ru.t1.dzelenin.tm.api.repository.IUserOwnedRepository;
import ru.t1.dzelenin.tm.model.AbstractUserOwnedModel;

public interface IUserOwnerService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {

}